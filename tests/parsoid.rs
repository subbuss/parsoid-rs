/*
Copyright (C) 2020-2021 Kunal Mehta <legoktm@member.fsf.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
use parsoid::prelude::*;
use parsoid::{map::IndexMap, Result};

fn build_client() -> Client {
    Client::new(
        "https://www.mediawiki.org/api/rest_v1",
        "parsoid-rs testing",
    )
    .unwrap()
}

#[tokio::test]
#[should_panic] // FIXME: doesn't work yet
async fn test_serialize() {
    let client = build_client();
    let html = client.get_raw("User:Legoktm").await.unwrap();
    let code = Wikicode::new(&html);
    assert_eq!(code.to_string(), html);
}

#[tokio::test]
async fn test_templates() -> Result<()> {
    let client = build_client();
    let code = client.get("MediaWiki").await?;
    let mut found = false;
    for template in code.filter_templates()? {
        if template.name_in_wikitext() == "Main page" {
            found = true;
        }
    }
    assert!(found);
    Ok(())
}

#[tokio::test]
async fn test_more_cases() -> Result<()> {
    let client = build_client();
    let code = client
            .transform_to_html(
                "{{1x|param<!--comment-->name=value|normal=value2}}{{#if:{{{1}}}|foo|bar}}",
            )
            .await?;
    let templates = code.filter_templates()?;
    // The second parser function wasn't included
    assert_eq!(templates.len(), 1);
    let temp = &templates[0];
    assert!(temp.is_template());
    assert!(!temp.is_parser_function());
    assert_eq!(temp.raw_name(), "./Template:1x");
    assert_eq!(temp.name(), "Template:1x");
    let mut params = IndexMap::new();
    params.insert("normal".to_string(), "value2".to_string());
    params.insert("paramname".to_string(), "value".to_string());
    assert_eq!(temp.get_params(), params);
    assert_eq!(temp.get_param("paramname"), Some("value".to_string()));
    assert_eq!(temp.get_param("notset"), None);
    assert_eq!(
        temp.get_param_in_wikitext("paramname"),
        Some("param<!--comment-->name".to_string())
    );
    assert_eq!(
        temp.get_param_in_wikitext("normal"),
        Some("normal".to_string())
    );
    assert_eq!(temp.get_param_in_wikitext("notset"), None);
    let pfs = code.filter_parser_functions()?;
    // The previous template wasn't included
    assert_eq!(pfs.len(), 1);
    let pf = &pfs[0];
    assert!(pf.is_parser_function());
    assert!(!pf.is_template());
    assert_eq!(pf.raw_name(), "if");
    Ok(())
}

#[tokio::test]
async fn test_template_mutation() -> Result<()> {
    let client = build_client();
    let original = "{{1x|foo=bar}}";
    let code = client.transform_to_html(original).await?;
    let mut templates = code.filter_templates()?;
    let temp = &mut templates[0];
    temp.set_param("new", "wikitext")?;
    let html = client.transform_to_wikitext(&code).await?;
    assert_eq!(html, "{{1x|foo=bar|new=wikitext}}".to_string());
    temp.remove_param("new")?;
    let new_html = client.transform_to_wikitext(&code).await?;
    assert_eq!(new_html, original.to_string());
    Ok(())
}

#[tokio::test]
async fn test_text_contents() -> Result<()> {
    let client = build_client();
    let code = client.get("User:Legoktm/parsoid-rs/strip_code").await?;
    assert_eq!(
        code.text_contents(),
        "This is some formatted code. Also a link.".to_string()
    );
    Ok(())
}

#[tokio::test]
async fn test_wikilinks() -> Result<()> {
    let client = build_client();
    let code = client.transform_to_html("[[Main Page|link text]]").await?;
    let links = code.filter_links();
    let link = &links[0];
    assert_eq!(link.raw_target(), "./Main_Page".to_string());
    assert_eq!(link.target(), "Main Page".to_string());
    assert_eq!(link.text_contents(), "link text".to_string());
    assert_eq!(
            &link.to_string(),
            "<a class=\"mw-redirect\" href=\"./Main_Page\" id=\"mwAw\" rel=\"mw:WikiLink\" title=\"Main Page\">link text</a>"
        );
    // Mutability
    link.set_target("MediaWiki");
    assert_eq!(link.raw_target(), "./MediaWiki".to_string());
    assert_eq!(link.target(), "MediaWiki".to_string());
    assert!(code.to_string().contains("href=\"./MediaWiki\""));
    let wikitext = client.transform_to_wikitext_raw(&code.to_string()).await?;
    assert_eq!(wikitext, "[[MediaWiki|link text]]".to_string());
    Ok(())
}

#[tokio::test]
async fn test_new_link() -> Result<()> {
    let client = build_client();
    let link = WikiLink::new("Foo", &Wikicode::new_text("bar"));
    assert_eq!(
        &link.to_string(),
        "<a href=\"./Foo\" rel=\"mw:WikiLink\">bar</a>"
    );
    let code = Wikicode::new("");
    code.append(&link);
    let new_wikitext = client.transform_to_wikitext(&code).await?;
    assert_eq!(new_wikitext, "[[Foo|bar]]".to_string());
    Ok(())
}

#[tokio::test]
async fn test_external_links() -> Result<()> {
    let client = build_client();
    let code = client
        .transform_to_html("[https://example.com Link content] ")
        .await?;
    let links = code.filter_external_links();
    let link = &links[0];
    assert_eq!(link.target(), "https://example.com".to_string());
    assert_eq!(link.text_contents(), "Link content".to_string());
    assert_eq!(
            &link.to_string(),
            "<a class=\"external text\" href=\"https://example.com\" id=\"mwAw\" rel=\"mw:ExtLink\">Link content</a>"
        );
    // Mutability
    link.set_target("https://wiki.example.org/foo?query=1");
    assert_eq!(
        link.target(),
        "https://wiki.example.org/foo?query=1".to_string()
    );
    let wikitext = client.transform_to_wikitext_raw(&code.to_string()).await?;
    assert_eq!(
        wikitext,
        "[https://wiki.example.org/foo?query=1 Link content] ".to_string()
    );
    Ok(())
}

#[tokio::test]
async fn test_comments() -> Result<()> {
    let client = build_client();
    let code = client.transform_to_html("<!--comment-->").await?;
    let comments = code.filter_comments();
    let comment = &comments[0];
    assert_eq!(comment.text(), "comment".to_string());
    // Surround with spaces for extra whitespace
    comment.set_text(" new ");
    assert_eq!(comment.text(), " new ".to_string());
    // Change is reflected in Wikicode serialization
    assert!(code.to_string().contains("<!-- new -->"));
    Ok(())
}

#[tokio::test]
async fn test_properties() -> Result<()> {
    let client = build_client();
    // FIXME: Use a real stable page
    let code = client.get("User:Legoktm/archive.txt").await?;
    assert_eq!(code.revision_id(), Some(2016428));
    assert_eq!(code.title(), Some("User:Legoktm/archive.txt".to_string()));
    Ok(())
}

#[tokio::test]
async fn test_iterators() -> Result<()> {
    let client = build_client();
    let code = client.transform_to_html("This is a [[sentence]].").await?;
    let link = code
        .descendants()
        .filter_map(|node| {
            dbg!(&node);
            node.as_wikilink()
        })
        .next()
        .unwrap();
    assert_eq!(link.raw_target(), "./Sentence".to_string());
    assert_eq!(link.target(), "Sentence".to_string());
    assert_eq!(link.text_contents(), "sentence".to_string());
    Ok(())
}

#[tokio::test]
async fn test_title() -> Result<()> {
    let client = build_client();
    let code = client.get("Project:Requests").await?;
    assert_eq!(code.title().unwrap(), "Project:Requests".to_string());
    Ok(())
}

#[tokio::test]
async fn test_sections() -> Result<()> {
    let client = build_client();
    let wikitext = r#"
...lead section contents...
== foo=bar ==
...section contents...
=== nested ===
...section contents...
"#;
    let code = client.transform_to_html(wikitext).await?;
    let sections = code.iter_sections();
    {
        let section = &sections[0];
        assert!(section.is_pseudo_section());
        assert_eq!(section.section_id(), 0);
        assert!(section.heading().is_none());
        assert!(section.is_editable());
    }
    {
        let section = &sections[1];
        assert!(!section.is_pseudo_section());
        assert_eq!(section.section_id(), 1);
        let heading = section.heading().unwrap();
        assert_eq!(heading.text_contents(), "foo=bar");
        assert_eq!(heading.get_level(), 2);
        assert!(section.is_editable());
    }
    {
        let section = &sections[2];
        assert!(!section.is_pseudo_section());
        assert_eq!(section.section_id(), 2);
        let heading = section.heading().unwrap();
        assert_eq!(heading.text_contents(), "nested");
        assert_eq!(heading.get_level(), 3);
        assert!(section.is_editable());
    }
    let new_heading = Heading::new(3, &Wikicode::new_text("inserted"))?;
    sections[2].insert_before(&new_heading);
    assert_eq!(
        r#"
...lead section contents...

== foo=bar ==
...section contents...

=== inserted ===

=== nested ===
...section contents...
"#,
        client.transform_to_wikitext(&code).await?
    );

    Ok(())
}

#[tokio::test]
async fn test_heading() -> Result<()> {
    let client = build_client();
    let heading = Heading::new(2, &Wikicode::new_text("Some text"))?;
    let code = Wikicode::new("");
    code.append(&heading);
    let wikitext = client.transform_to_wikitext(&code).await?;
    assert_eq!(&wikitext, "== Some text ==\n");

    Ok(())
}

#[test]
#[should_panic]
fn test_invalid_heading() {
    Heading::new(7, &Wikicode::new_text("foo")).unwrap();
}

#[tokio::test]
async fn test_category() -> Result<()> {
    let client = build_client();
    let category = Category::new("Category:Foo bar", Some("Bar baz#quux"));
    let code = Wikicode::new("");
    code.append(&category);
    let wikitext = client.transform_to_wikitext(&code).await?;
    assert_eq!(&wikitext, "[[Category:Foo bar|Bar baz#quux]]");
    let cats: Vec<_> = code
        .inclusive_descendants()
        .filter_map(|node| node.as_category())
        .collect();
    let cat = &cats[0];
    assert_eq!(&cat.category(), "Category:Foo bar");
    assert_eq!(cat.sort_key(), Some("Bar baz#quux".to_string()));
    cat.set_category("Category:Bar baz");
    cat.set_sort_key(None);
    assert_eq!(cat.sort_key(), None);
    assert_eq!(
        &cat.to_string(),
        "<link href=\"./Category:Bar_baz\" rel=\"mw:PageProp/Category\">"
    );
    let new_wikitext = client.transform_to_wikitext(&code).await?;
    assert_eq!(&new_wikitext, "[[Category:Bar baz]]");

    Ok(())
}

#[tokio::test]
async fn test_language_link() -> Result<()> {
    let client = build_client();
    let link = LanguageLink::new("https://en.wikipedia.org/wiki/Foo");
    let code = Wikicode::new("");
    code.append(&link);
    let wikitext = client.transform_to_wikitext(&code).await?;
    assert_eq!(&wikitext, "[[en:Foo]]");
    link.set_target("https://de.wikipedia.org/wiki/Foo");
    assert_eq!(&link.target(), "https://de.wikipedia.org/wiki/Foo");
    let wikitext2 = client.transform_to_wikitext(&code).await?;
    assert_eq!(&wikitext2, "[[de:Foo]]");
    Ok(())
}

#[tokio::test]
async fn test_behavior_switch() -> Result<()> {
    let client = build_client();
    let code = Wikicode::new("");
    code.append(&BehaviorSwitch::new("toc", None));
    code.append(&BehaviorSwitch::new("displaytitle", Some("foo")));
    let wikitext = client.transform_to_wikitext(&code).await?;
    assert_eq!(&wikitext, "__TOC__\n{{DISPLAYTITLE:foo}}\n");
    let switches: Vec<_> = code
        .inclusive_descendants()
        .filter_map(|node| node.as_behavior_switch())
        .collect();
    {
        let switch = &switches[0];
        assert_eq!(&switch.property(), "toc");
        assert_eq!(switch.content(), None);
    }
    {
        let switch = &switches[1];
        assert_eq!(&switch.property(), "displaytitle");
        assert_eq!(switch.content(), Some("foo".to_string()));
    }
    Ok(())
}

#[tokio::test]
async fn test_redirect() -> Result<()> {
    let client = build_client();
    let code = Wikicode::new("");
    code.append(&Redirect::new("Foo"));
    let redirect = code.get_redirect().unwrap();
    assert_eq!(redirect.target(), "Foo".to_string());
    assert_eq!(redirect.raw_target(), "./Foo".to_string());
    assert!(!redirect.is_external());
    let wikitext = client.transform_to_wikitext(&code).await?;
    assert_eq!(&wikitext, "#REDIRECT [[Foo]]");

    Ok(())
}

#[tokio::test]
async fn test_external_redirect() -> Result<()> {
    let client = build_client();
    let code = client
        .transform_to_html("#REDIRECT [[w:Main Page]]")
        .await?;
    dbg!(&code.to_string());
    dbg!(&code.get_redirect());
    let redirect = code.get_redirect().expect("not a redirect");
    assert!(redirect.is_external());
    assert_eq!(
        redirect.target(),
        "https://en.wikipedia.org/wiki/Main%20Page".to_string()
    );
    assert_eq!(
        redirect.raw_target(),
        "https://en.wikipedia.org/wiki/Main%20Page".to_string()
    );
    Ok(())
}

#[tokio::test]
async fn test_detach() -> Result<()> {
    let client = build_client();
    let code = client.transform_to_html("This is a [[link]].").await?;
    for link in code.filter_links() {
        link.detach();
    }

    let wikitext = client.transform_to_wikitext(&code).await?;
    assert_eq!(&wikitext, "This is a .");

    Ok(())
}

#[tokio::test]
async fn test_section_prepend() -> Result<()> {
    let client = build_client();
    let code = client.transform_to_html("== Section ==\n[[foo]]").await?;
    let section = code.iter_sections()[1].clone();
    // Need to insert_after the heading otherwise parsoid will move the content before
    // the <section> tag
    section
        .heading()
        .unwrap()
        .insert_after(&WikiLink::new("./Bar", &Wikicode::new_text("Bar")));
    let wikitext = client.transform_to_wikitext(&code).await?;
    assert_eq!(&wikitext, "== Section ==\n[[Bar]]\n[[foo]]");
    Ok(())
}

#[tokio::test]
async fn test_parser_functions() -> Result<()> {
    let client = build_client();
    let code = client
        .get("User:Legoktm/parsoid-rs/parser function")
        .await?;
    let templates = code.filter_parser_functions()?;
    let temp = &templates[0];
    assert!(temp.is_parser_function());
    // name is literally the entire function
    assert_eq!(
        temp.name_in_wikitext(),
        "#expr:{{formatnum:{{NUMBEROFUSERS}}|R}}+100"
    );
    assert_eq!(temp.raw_name(), "expr");
    // identical to normalized_name
    assert_eq!(temp.name(), "expr");
    Ok(())
}

#[tokio::test]
async fn test_question() -> Result<()> {
    let client = build_client();
    let code = client.get("User:Legoktm/parsoid-rs/question").await?;
    let templates = code.filter_templates()?;
    let temp = &templates[0];
    assert_eq!(&temp.name(), "Template:But why?");
    Ok(())
}
