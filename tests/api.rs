/*
Copyright (C) 2020-2021 Kunal Mehta <legoktm@member.fsf.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
use parsoid::{Client, Error, Result};

#[tokio::test]
async fn test_get() -> Result<()> {
    let client = Client::new(
        "https://www.mediawiki.org/api/rest_v1",
        "parsoid-rs testing",
    )?;
    let html = client.get_raw("MediaWiki").await?;
    assert!(html.contains("Special:MyLanguage/Download"));
    match client.get("ThisPageDoesNotExist").await {
        Err(Error::PageDoesNotExist(title)) => {
            assert_eq!("ThisPageDoesNotExist".to_string(), title);
        }
        _ => {
            panic!("Test did not fail with Error::PageDoesNotExist()");
        }
    }
    Ok(())
}

#[tokio::test]
async fn test_get_revision() -> Result<()> {
    let client = Client::new(
        "https://en.wikipedia.org/api/rest_v1",
        "parsoid-rs testing",
    )?;
    let code = client.get_revision("HomePage", 908493298).await?;
    assert_eq!(
        code.text_contents(),
        "This is the new WikiPedia!".to_string()
    );
    assert_eq!(code.revision_id(), Some(908493298));
    Ok(())
}

#[tokio::test]
async fn test_transform_to_html() -> Result<()> {
    let client = Client::new(
        "https://www.mediawiki.org/api/rest_v1",
        "parsoid-rs testing",
    )?;
    let html = client
        .transform_to_html_raw("{{1x|This is HTML now}}")
        .await?;
    assert!(html.contains("This is HTML now"));
    Ok(())
}

#[tokio::test]
async fn test_transform_to_wikitext() -> Result<()> {
    let client = Client::new(
        "https://www.mediawiki.org/api/rest_v1",
        "parsoid-rs testing",
    )?;
    let wikitext = client
        .transform_to_wikitext_raw(
            "<a rel=\"mw:WikiLink\" href=\"./Foo\">Foo bar</a>",
        )
        .await?;
    assert_eq!(wikitext, "[[Foo|Foo bar]]".to_string());
    Ok(())
}
