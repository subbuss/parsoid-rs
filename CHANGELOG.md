## 0.5.0 / 2020-02-20
* [BREAKING] `filter_comments()` now returns `Vec<Comment>`.
* [BREAKING] `Wikicode::new_fragment()` has been replaced by
  `Wikicode::new_node()` and `Wikicode::new_text()`
* [BREAKING] `Heading::new()` takes `&NodeRef` as the second parameter instead
  of a `&str`.
* [BREAKING] `filter_templates()` will only return templates. For parser
  functions use `filter_parser_functions()`.
* [BREAKING] Rename most target/link functions guide people into the
  "pretty" formats by default, providing `raw_` functions if you need it.
* Various link functions take `&NodeRef` as the text parameter.
* Add `Client::get_revision()` to fetch old revisions.
* Mark `Error` as `#[non_exhaustive]`.
* Add request logging using `log`.

## 0.5.0-alpha.1 / 2021-02-11
* [BREAKING] `Template` now represents a list of sibling nodes instead of just one
  * To access the underlying nodes, use `temp.as_nodes()` instead of the now 
    removed `temp.get_node()`. Helpers for most `.attach()`, `.prepend()`, etc.
    operations are available in `Template`.
* [BREAKING] `filter_links()`, `filter_external_links()` now return
  `Vec<Wikinode>`.
* Update to [HTML spec 2.2.0](https://www.mediawiki.org/wiki/Specs/HTML/2.2.0),
  no practical changes for us.
* Make `Wikinode::` ... `prepend()`, `insert_before()`, `insert_after()` usable
* Add `Template::new_simple()` for creation without parameters

## 0.4.2 / 2021-04-07
* Add helpers to get a "pretty" page title for links and templates

## 0.4.1 / 2021-04-06
* Parsoid's `Error` is now available as `parsoid::Error`
* Add specific `Error::PageDoesNotExist` type
* Add specific `Error::InvalidEtag` type

## 0.4.0 / 2021-04-05

* Use dependencies that use Tokio 1.0
* Fix roundtripping of some Cite errors

## 0.3.0-alpha.3 / 2020-11-11
* First usable release
