/*
Copyright (C) 2020-2021 Kunal Mehta <legoktm@member.fsf.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
use anyhow::Result;
use parsoid::Client;
use std::io::{self, Read};

#[tokio::main]
async fn main() -> Result<()> {
    let client = Client::new(
        "https://www.mediawiki.org/api/rest_v1",
        "parsoid-rs testing",
    )?;
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)?;
    let html = client.transform_to_html_raw(&input).await?;
    println!("{}", &html);
    Ok(())
}
