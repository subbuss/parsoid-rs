/*
Copyright (C) 2020-2021 Kunal Mehta <legoktm@member.fsf.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
//! Error type
use thiserror::Error as ThisError;

/// Errors
#[non_exhaustive]
#[derive(ThisError, Debug)]
pub enum Error {
    /// HTTP request errors, forwarded from `reqwest`
    #[cfg(feature = "http")]
    #[error("HTTP error: {0}")]
    HttpError(#[from] reqwest::Error),
    /// If the requested page doesn't exist
    #[cfg(feature = "http")]
    #[error("The \"{0}\" page does not exist")]
    PageDoesNotExist(String),
    #[cfg(feature = "http")]
    #[error("The etag for this request is missing or invalid")]
    InvalidEtag,
    /// If the data-mw attribute has invalid JSON, forwarded from `serde_json`
    #[error("Invalid JSON: {0}")]
    InvalidJSON(#[from] serde_json::Error),
    #[error("Heading levels must be between 1 and 6, '{0}' was provided")]
    InvalidHeadingLevel(u32),
}
