parsoid-rs
============
[![crates.io](https://img.shields.io/crates/v/parsoid.svg)](https://crates.io/crates/parsoid)
[![docs.rs](https://docs.rs/parsoid/badge.svg)](https://docs.rs/parsoid)
[![pipeline status](https://gitlab.com/legoktm/parsoid-rs/badges/master/pipeline.svg)](https://gitlab.com/legoktm/parsoid-rs/-/commits/master)
[![coverage report](https://gitlab.com/legoktm/parsoid-rs/badges/master/coverage.svg)](https://legoktm.gitlab.io/parsoid-rs/coverage/)

The `parsoid` crate is a wrapper around [Parsoid HTML](https://www.mediawiki.org/wiki/Specs/HTML/2.2.0)
that provides convenient accessors for processing and extraction.

See the [full documentation](https://docs.rs/parsoid/) (docs for [master](https://legoktm.gitlab.io/parsoid-rs/parsoid/)).

## Testing
Use the `build_corpus` example to download the first 500 featured articles
on the English Wikipedia to create a test corpus.

The `featured_articles` example will iterate through those downloaded examples
to test the parsing code, clean roundtripping, etc.

## License
parsoid-rs is (C) 2020-2021 Kunal Mehta, released under the GPL v2 or any later version,
see COPYING for details.
